dnl Copyright (c) 2012  Murray Cumming <murrayc@murrayc.com>
dnl This file is part of grilomm.

_CONVERSION(`GrlRegistry*',`Glib::RefPtr<Registry>',`Glib::wrap($3)')
_CONVERSION(`GrlPlugin*',`Glib::RefPtr<Plugin>',`Glib::wrap($3)')
_CONVERSION(`GrlPlugin*',`Glib::RefPtr<const Plugin>',`Glib::wrap($3)')
