#!/bin/bash

# grilomm/codegen/generate_defs_and_docs.sh

# This script must be executed from directory grilomm/codegen.

# Assumed directory structure:
#   glibmm/tools/defs_gen/docextract_to_xml.py
#   glibmm/tools/defs_gen/h2def.py
#   glibmm/tools/enum.pl
#   grilo/src/*.h
#   grilo/src/*.c
#   grilomm/codegen/extradefs/generate_extra_defs

# Generated files:
#   grilomm/grilo/src/grilo_docs.xml
#   grilomm/grilo/src/grilo_enum.defs
#   grilomm/grilo/src/grilo_method.defs
#   grilomm/grilo/src/grilo_signal.defs

GLIBMM_TOOLS_DIR=../../glibmm/tools
GRILO_DIR=../../grilo
GRILOMM_GRILO_SRC_DIR=../grilo/src

$GLIBMM_TOOLS_DIR/defs_gen/docextract_to_xml.py \
  -s $GRILO_DIR/src \
  >$GRILOMM_GRILO_SRC_DIR/grilo_docs.xml

$GLIBMM_TOOLS_DIR/enum.pl \
  $GRILO_DIR/src/*.h \
  >$GRILOMM_GRILO_SRC_DIR/grilo_enum.defs

$GLIBMM_TOOLS_DIR/defs_gen/h2def.py \
  $GRILO_DIR/src/*.h \
  >$GRILOMM_GRILO_SRC_DIR/grilo_method.defs

extradefs/generate_extra_defs \
  >$GRILOMM_GRILO_SRC_DIR/grilo_signal.defs

