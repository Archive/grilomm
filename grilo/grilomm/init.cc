/* Copyright 2012  Murray Cumming  <murrayc@murrayc.com>
 *
 * This file is part of grilomm.
 *
 * grilomm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * grilomm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm/init.h>
#include <grilommconfig.h>
#include <grilomm/wrap_init.h>
#include <grilo.h>

namespace Grilo
{

void init()
{
  grl_init(0, 0);

  Glib::init(); //Sets up the g type system and the Glib::wrap() table.
  wrap_init(); //Tells the Glib::wrap() table about the grilomm classes.
}


} //namespace Grilo
