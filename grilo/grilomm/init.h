/* Copyright 2012  Murray Cumming  <murrayc@murrayc.com>
 *
 * This file is part of grilomm.
 *
 * grilomm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * grilomm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GRILOMM_INIT_H
#define _GRILOMM_INIT_H

namespace Grilo
{

/** Initialize grilomm
 */
void init();

} //namespace Grilo

#endif //_GRILOMM_INIT_H

