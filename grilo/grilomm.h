/* Copyright (c) 2012  Murray Cumming <murrayc@murrayc.com>
 *
 * This file is part of grilomm.
 *
 * grilomm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * grilomm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GRILOMM_H_INCLUDED
#define GRILOMM_H_INCLUDED

/** @mainpage grilomm Reference Manual
 *
 * @section description Description
 *
 * The grilomm C++ binding provides a C++ interface on top of the grilo
 * C library.
 *
 * @section overview Overview
 *
 * [...]
 *
 * @section use Use
 *
 * To use grilomm in your C++ application, include the central header file
 * <tt>\<grilomm.h\></tt>.  The grilomm package ships a @c pkg-config
 * file with the correct include path and link command-line for the compiler.
 */

#include <grilommconfig.h>
#include <grilomm/init.h>
#include <grilomm/plugin.h>
#include <grilomm/registry.h>
#include <grilomm/source.h>

/** @example example/example.cc
 * A grilomm example program.
 */

#endif /* !GRILOMM_H_INCLUDED */
