/* Copyright (c) 2012  Murray Cumming <murrayc@murrayc.com>
 *
 * This file is part of grilomm.
 *
 * grilomm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * grilomm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grilomm.h>
#include <iostream>

int main(int, char**)
{
  Grilo::init();

  const Glib::RefPtr<Grilo::Registry> registry = Grilo::Registry::get_default();
  if(!registry)
  {
    std::cerr << "Grilo::Registry::get_default() returned null." << std::endl;
    return EXIT_FAILURE;
  }

  typedef std::vector<Glib::RefPtr<Grilo::Source> > typeVecSources;
  const typeVecSources sources = registry->get_sources();

  std::cout << "Sources:" << std::endl;
  for(typeVecSources::const_iterator iter = sources.begin(); iter != sources.end(); ++iter)
  {
    const Glib::RefPtr<Grilo::Source> source = *iter;
    if(!source)
      continue;

    std::cout << source->get_name() << std::endl;
  }

  return EXIT_SUCCESS;
}
